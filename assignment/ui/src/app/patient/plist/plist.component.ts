import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../service/booking.service';
import * as moment from 'moment';

@Component({
  selector: 'app-plist',
  templateUrl: './plist.component.html',
  styleUrls: ['./plist.component.css']
})
export class PlistComponent implements OnInit {

  booking_data: any;

  constructor(
    private _bookingService: BookingService,
  ) { }

  ngOnInit(): void {
    this.getBookingDetails()
  }

  checkStatus(date: any, from_time: any, to_time: any){
    var todayDate = moment(new Date(), 'DD-MM-YYYY');
    var pastDate = moment(date, 'DD-MM-YYYY');
    
    if (todayDate.isBefore(pastDate)) {
      return 'Not Completed';
    }else{
      if(todayDate.format('DD-MM-YYYY') == pastDate.format('DD-MM-YYYY')){
        var format = 'hh:mm:ss'
        let start = moment(moment('09:00 AM', ["h:mm A"]).format("HH:mm"), format);
        let end = moment(moment(to_time, ["h:mm A"]).format("HH:mm"), format);
        let current = moment(moment(new Date(), ["h:mm A"]).format("HH:mm"), format);
        if(current.isBetween(start, end)){
          return 'Not Completed';
        } else {
          return 'Completed';
        }
      } else {
        return 'Completed';
      }
    }
  }

  getBookingDetails(){
    this._bookingService
      .patientBookings(localStorage.getItem('_id'))
      .subscribe(
        (result) => {
          if(result.result){
            this.booking_data = result.data;
            console.log(this.booking_data)
          }
        },
        (error) => {

        }
      )
  }

}
