import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { PopupComponent } from './popup/popup.component';
import { SlotService } from '../../service/slot.service';
import { BookingService } from '../../service/booking.service';
import { ToastrService } from 'ngx-toastr';

export interface DialogData {
  id: any;
  from_date: any;
  to_date: any;
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  selectedDate = moment();
  from_date: any;
  to_date: any;
  slot_data: any;


  constructor(
    public dialog: MatDialog,
    private _slotService: SlotService,
    private _bookingService: BookingService,
    public toasterService: ToastrService,
  ) { }


  ngOnInit(): void {
    this.getSlotDetails(moment(this.selectedDate).format('DD-MM-YYYY'))
  }

  checkPastDate(slot: any){
    var todayDate = moment(new Date(), 'DD-MM-YYYY');
    var pastDate = moment(this.selectedDate, 'DD-MM-YYYY');
    
    if (todayDate.isBefore(pastDate)) {
      return false;
    }else{
      if(todayDate.format('DD-MM-YYYY') == pastDate.format('DD-MM-YYYY')){
        var format = 'hh:mm:ss'
        let start = moment(moment('09:00 AM', ["h:mm A"]).format("HH:mm"), format);
        let end = moment(moment(slot.to_time, ["h:mm A"]).format("HH:mm"), format);
        let current = moment(moment(new Date(), ["h:mm A"]).format("HH:mm"), format);
        if(current.isBetween(start, end)){
          return false;
        } else {
          console.log('N')
          return true;
        }
        //return false;
      } else {
        return true;
      }
    }
  }

  getSlotDetails(date: any){
    this._slotService
      .slotData(date)
      .subscribe(
        (result) => {
          if(result.result){
            this.slot_data = result.data;
            console.log(this.slot_data)
          }
        },
        (error) => {

        }
      )
  }

  openDialog(slot: any): void {
    const dialogRef = this.dialog.open(PopupComponent, {
      width: '250px',
      data: {from_time: moment(slot.from_time, ["h:mm A"]).format("HH:mm"), to_time: moment(slot.to_time, ["h:mm A"]).format("HH:mm"), date: moment(this.selectedDate).format('DD-MM-YYYY'), comment: ''}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.bookingData(slot, result);
      }
    });
  }

  bookingData(slot: any, result: any){
    var format = 'hh:mm:ss'
    let start = moment(result.from_time, format);
    let end = moment(result.to_time, format);
    var duration = moment.duration(end.diff(start));
    var minutes = duration.asMinutes()%60;
    let bookedData = {
      date: result.date,
      from_time: moment(result.from_time, ["h:mm A"]).format("h:mm A"),
      to_time: moment(result.to_time, ["h:mm A"]).format("h:mm A"),
      duration: minutes + ' Mins',
      comment: result.comment,
      slot: slot._id,
      patient: localStorage.getItem('_id')
    };
    
    this._bookingService
      .bookingSlot(bookedData)
      .subscribe(
        (result) => {
          if(result.result){
            this.toasterService.success(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.getSlotDetails(moment(this.selectedDate).format('DD-MM-YYYY'))
          } else {
            this.toasterService.error(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
          }
        },
        (error) => {
          this.toasterService.error(
            'Please Try Again.',
            '',
            {
                positionClass: 'toast-top-center',
                timeOut: 3000
            }
          );
        }     
      );
  }

  dateChanged(event: any) {
    this.getSlotDetails(moment(this.selectedDate).format('DD-MM-YYYY'))
    //console.log(moment(this.selectedDate).format("DD/MM/YYYY"));
  }

}
