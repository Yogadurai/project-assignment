import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {BookingComponent} from '../booking.component';
import * as moment from 'moment';

export interface DialogData {
  date: any;
  from_time: any;
  to_time: any;
  comment: any;
}

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  bookDisabled = false;
  tempFromTime: any;
  tempToTime: any;
  slotAvailable = false;
  
  constructor(
    public dialogRef: MatDialogRef<BookingComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      this.tempFromTime = data.from_time
      this.tempToTime = data.to_time
      console.log('mat data', data)
    }

  ngOnInit(): void {
  }

  cmdChange(cmd: any){
    this.data.comment = cmd;
  }

  onChangeTime(type: any, event: any){
    if(type == 'S'){
      var format = 'hh:mm:ss'
      let start = moment(event, format);
      let end = moment(this.data.to_time, format).add(1, 'minutes');
      let leadStart = moment(this.tempFromTime, format).add(-1, 'minutes');
      if(start.isBetween(leadStart, end)){
        this.data.from_time = event;
        this.slotAvailable = false;
        this.bookDisabled = false;
      } else {
        this.slotAvailable = true;
        this.bookDisabled = true;
      }
    } else {
      var format = 'hh:mm:ss'
      let end = moment(event, format);
      let start = moment(this.data.from_time, format).add(-1, 'minutes');
      let leadEnd = moment(this.tempToTime, format).add(+1, 'minutes');
      if(end.isBetween(start, leadEnd)){
        this.data.to_time = event;
        this.slotAvailable = false;
        this.bookDisabled = false;
      } else {
        this.slotAvailable = true;
        this.bookDisabled = true;
      }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
