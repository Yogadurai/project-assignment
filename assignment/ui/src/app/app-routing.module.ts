import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './common/signup/signup.component';
import { LoginComponent } from './common/login/login.component';
import { ListComponent } from './doctor/list/list.component';
import { PlistComponent } from './patient/plist/plist.component';
import { BookingComponent } from './patient/booking/booking.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'signup', component: SignupComponent},
  {
    path: 'doctor',
    children: [
      {
        path:  'list',
        component:  ListComponent
      },
    ]
  },
  {
    path: 'patient',
    children: [
      {
        path:  'list',
        component:  PlistComponent
      },
      {
        path:  'booking',
        component:  BookingComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
