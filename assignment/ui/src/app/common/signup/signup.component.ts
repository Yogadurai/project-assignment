import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  loading = false;

  registerForm = this.fb.group({
    name: new FormControl('', [Validators.required]),
    gendar: new FormControl('M', [Validators.required]),
    age: new FormControl('', [Validators.required]),
    contact: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private fb: FormBuilder,
    public toasterService: ToastrService,
    private el: ElementRef,
    private _userService: UserService,
    private router: Router,
  ) {
    let checkLoginToken = localStorage.getItem('token');
    let checkLoginUserType = localStorage.getItem('user_type');
    if(checkLoginToken){
      if(checkLoginUserType == 'P'){
        this.router.navigate(['/patient/booking']);
      } else {
        this.router.navigate(['/doctor/list']);
      }
    } 
  }

  ngOnInit(): void {
    
  }

  get trrowErrors() { return this.registerForm.controls; }
  
  onSubmit(){
    if (this.registerForm.invalid) {
      for (const key of Object.keys(this.registerForm.controls)) {
          if (this.registerForm.controls[key].invalid) {
            const invalidControl = this.el.nativeElement.querySelector('[formcontrolname=' + key + ']');
            invalidControl.focus();
            break;
          }
        }
      return;
    }
    this.loading = true;

    let registerFormData = this.registerForm.value;

    let createDatas = {
      name: registerFormData.name,
      gendar: registerFormData.gendar,
      age: registerFormData.age,
      contact: registerFormData.contact,
      address: registerFormData.address,
      password: registerFormData.password,
    }

    this._userService
      .registerDetails(createDatas)
      .subscribe(
        (result) => {
          if(result.result){
            this.toasterService.success(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.loading = false;
            this.router.navigate(['/']);
          } else {
            this.toasterService.error(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.loading = false;
            this.router.navigate(['/signup']);
          }
        },
        (error) => {
          this.toasterService.error(
            'Please Try Again.',
            '',
            {
                positionClass: 'toast-top-center',
                timeOut: 3000
            }
          );
          this.loading = false;
          this.router.navigate(['/signup']);
        }     
      );
  }

}
