import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { Location } from "@angular/common";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  title = 'Doctor';
  isAuthenticated: any;
  time = new Date();
  intervalId: any;
  userType: any;
  locationPath = '/';

  constructor(
    private router: Router,
    public toasterService: ToastrService,
    location: Location,
  ) { 
    
    router.events.subscribe(val => {
      if (location.path() != "") {
        this.locationPath = location.path();
      } else {
        this.locationPath = '/';
      }
    });
  }

  ngOnInit(): void {
    this.intervalId = setInterval(() => {
      this.time = new Date();
      this.isAuthenticated = localStorage.getItem('token') ? true : false;
      this.userType = localStorage.getItem('user_type');
    }, 1000);
  }

  ngOnDestroy() {
    clearInterval(this.intervalId);
  }

  async logout(): Promise<void> {
    localStorage.removeItem('user_type');
    localStorage.removeItem('_id');
    localStorage.removeItem('name');
    localStorage.removeItem('gendar');
    localStorage.removeItem('age');
    localStorage.removeItem('contact');
    localStorage.removeItem('address');
    localStorage.removeItem('token');
    
    this.toasterService.success(
      'Logged Out Successful',
      '',
      {
          positionClass: 'toast-top-center',
          timeOut: 3000
      }
    );
    this.router.navigate(['/']);
  }

}
