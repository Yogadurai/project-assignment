import { Component, OnInit, ElementRef } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from '../../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;

  loginForm = this.fb.group({
    mobile: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private fb: FormBuilder,
    public toasterService: ToastrService,
    private el: ElementRef,
    private _userService: UserService,
    private router: Router,
  ) {
    let checkLoginToken = localStorage.getItem('token');
    let checkLoginUserType = localStorage.getItem('user_type');
    if(checkLoginToken){
      if(checkLoginUserType == 'P'){
        this.router.navigate(['/patient/booking']);
      } else {
        this.router.navigate(['/doctor/list']);
      }
    } 
  }

  ngOnInit(): void {
  }

  get trrowErrors() { return this.loginForm.controls; }

  onSubmit(){
    if (this.loginForm.invalid) {
      for (const key of Object.keys(this.loginForm.controls)) {
          if (this.loginForm.controls[key].invalid) {
            const invalidControl = this.el.nativeElement.querySelector('[formcontrolname=' + key + ']');
            invalidControl.focus();
            break;
          }
        }
      return;
    }
    this.loading = true;

    let loginFormData = this.loginForm.value;

    let createDatas = {
      mobile: loginFormData.mobile,
      password: loginFormData.password,
    }

    this._userService
      .login(createDatas)
      .subscribe(
        (result) => {
          if(result.result){
            this.logSuccess(result.data);
            this.toasterService.success(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.loading = false;
            if(result.data.user_data.user_type === 'P'){
              this.router.navigate(['/patient/booking']);
            } else {
              this.router.navigate(['/doctor/list']);
            }
          } else {
            this.logFail();
            this.toasterService.error(
              result.message,
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.loading = false;
          }
        },
        (error) => {
          this.logFail();
            this.toasterService.error(
              'Please Try Again',
              '',
              {
                  positionClass: 'toast-top-center',
                  timeOut: 3000
              }
            );
            this.loading = false;
        }
      );
    console.log(this.loginForm)
  }

  logFail(){
    localStorage.removeItem('user_type');
    localStorage.removeItem('_id');
    localStorage.removeItem('name');
    localStorage.removeItem('gendar');
    localStorage.removeItem('age');
    localStorage.removeItem('contact');
    localStorage.removeItem('address');
    localStorage.removeItem('token');
  }

  logSuccess(data: any){
    localStorage.setItem(
      'user_type',
      data.user_data.user_type
    );
    localStorage.setItem(
      '_id',
      data.user_data._id
    );
    localStorage.setItem(
      'name',
      data.user_data.name
    );
    localStorage.setItem(
      'gendar',
      data.user_data.gendar
    );
    localStorage.setItem(
      'age',
      data.user_data.age
    );
    localStorage.setItem(
      'contact',
      data.user_data.contact
    );
    localStorage.setItem(
      'address',
      data.user_data.address
    );
    localStorage.setItem(
      'token',
      data.token
    );
  }

}
