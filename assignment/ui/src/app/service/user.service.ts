import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class UserService {

    private baseUrlRegister: string = '/api/patient/register';
    private baseUrlLogin: string = '/api/login';

    constructor(private http: HttpClient,
    ) {}

    registerDetails(user_details: any){
        return this.http
        .post<any>(this.baseUrlRegister,
            user_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
        );
    }

    login(login_details: any){
        return this.http
        .post<any>(this.baseUrlLogin,
            login_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
        );
    }


}

