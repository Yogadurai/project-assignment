import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class SlotService {
    private baseUrlSlotList: string = '/api/patient/slots';

    constructor(private http: HttpClient,
        ) {}

    slotData(date: any){
        return this.http
            .get<any>(this.baseUrlSlotList + '/' + date,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'authorization': 'bearer '+localStorage.getItem('token')
                    }
                })
                .pipe(
                    shareReplay(),
                    map(
                        (result) => {
                            return result;
                        }
                    )
                );
    }
}