import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class BookingService {
    private baseUrlBooking: string = '/api/patient/booking';
    private baseUrlPatientBooking: string = '/api/patient/plist';
    private baseUrlDoctorBooking: string = '/api/patient/dlist';

    constructor(private http: HttpClient,
        ) {}

    bookingSlot(booking_details: any){
        return this.http
        .post<any>(this.baseUrlBooking,
            booking_details,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'bearer '+localStorage.getItem('token')
                }
            })
            .pipe(
                map((result) => {
                    return result;
                })
        );
    }

    patientBookings(id: any){
        return this.http
        .get<any>(this.baseUrlPatientBooking + '/' + id,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'bearer '+localStorage.getItem('token')
                }
            })
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    }
                )
            );
    }

    doctorBookings(){
        return this.http
        .get<any>(this.baseUrlDoctorBooking,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': 'bearer '+localStorage.getItem('token')
                }
            })
            .pipe(
                shareReplay(),
                map(
                    (result) => {
                        return result;
                    }
                )
            );
    }
}